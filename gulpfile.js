var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var coffee = require('gulp-coffee');
var es = require('event-stream');

gulp.task('default', function() {
    var jsFromCoffeeScript = gulp.src('src/*.coffee')
        .pipe(coffee());
		    // no gulp.dest() since it will bee kept in memory
    var js = gulp.src('src/*.js');
				// no gulp.dest() since it will bee kept in memory

    // merge array of jsFromCoffee and js
    return es.merge(jsFromCoffeeScript, js)
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
    gulp.watch('src/*.{js,coffee}', ['default']);
});
